<?php

function drufoo_admin_settings_form() {
  $form['drufoo_username'] = array(
    '#type' => 'textfield',
    '#title' => t('User name'),
    '#description' => t('Your Wufoo account user name'),
    '#default_value' => variable_get('drufoo_username', ''),
    '#required' => TRUE
  );
  $form['drufoo_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('Your Wufoo API Key.'),
    '#default_value' => variable_get('drufoo_key', ''),
    '#required' => TRUE
  );
  return system_settings_form($form);
}

function drufoo_admin_forms_page() {
  $forms_manager = drufoo_get_manager('forms');
  if (!$forms_manager) {
    drupal_not_found();
    exit();
  }
  $forms = array();
  foreach ($forms_manager->getForms() as $form) {
    $details = $form->getDetails();
    $forms[$form->hash] = $details;
  }
  return theme('drufoo_admin_forms_page', $forms);
}

function drufoo_admin_form_fields($hash) {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('Forms'), 'admin/drufoo/forms');
  drupal_set_breadcrumb($breadcrumb);

  $fields_manager = drufoo_get_manager('fields');
  $fields = $fields_manager->getFields($hash);
  return theme('drufoo_admin_form_fields', $fields->getDetails());
}

function drufoo_admin_form_entries($hash) {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('Forms'), 'admin/drufoo/forms');
  drupal_set_breadcrumb($breadcrumb);

  $entries_manager = drufoo_get_manager('entries');

  // Attach a pager.
  $entries_manager->attachPager();

  // Attach filters.
  $entries_manager->attachFilter();

  $entries = $entries_manager->getEntries($hash);
  $entry_array = array();
  foreach ($entries as $entry) {
    $entry_array[] = $entry->getDetails();
  }
  $fields_manager = drufoo_get_manager('fields');
  $fields = $fields_manager->getFields($hash);
  $fields_details = $fields->getDetails();
  return drupal_get_form('drufoo_entries_filter_form', $fields_details) . theme('drufoo_admin_form_entries', $hash, $entry_array, $fields_details);
}

function drufoo_admin_entry_view($hash, $entry) {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('Administer'), 'admin');
  $breadcrumb[] = l(t('Drufoo'), 'admin/drufoo');
  $breadcrumb[] = l(t('Forms'), 'admin/drufoo/forms');
  $breadcrumb[] = l(t('Entries'), 'admin/drufoo/forms/'. $hash .'/entries');
  drupal_set_breadcrumb($breadcrumb);

  $fields_manager = drufoo_get_manager('fields');
  $fields = $fields_manager->getFields($hash);
  return theme('drufoo_admin_entry_view', $entry->getDetails(), $fields->getDetails());
}

function drufoo_admin_entry_edit_form($form_state, $hash, $entry) {
  $fields_manager = drufoo_get_manager('fields');
  $fields = $fields_manager->getFields($hash)->getDetails();

  foreach ($fields as $key => $field) {
    if (!in_array($key, array('EntryId', 'DateCreated', 'CreatedBy', 'LastUpdated', 'UpdatedBy'))) {
      
    }
  }
  return array();
}

function drufoo_admin_reports_page() {
  $reports_manager = drufoo_get_manager('reports');
  $reports = $reports_manager->getReports();
  $reports_array = array();
  foreach ($reports as $report) {
    $reports_array[] = $report->getDetails();
  }
  return theme('drufoo_admin_reports_page', $reports_array);
}

function drufoo_admin_report_view($report) {
  $widgets_manager = drufoo_get_manager('widgets');
  $widgets = $widgets_manager->getWidgets($report->hash);
  $snippets = array();
  if (!empty($widgets)) {
    foreach ($widgets as $widget) {
      $snippets[] = $widget->getSnippet();
    }
  }
  return theme('drufoo_admin_report_view', $report->getDetails(), $snippets);
}

function theme_drufoo_admin_forms_page($forms) {
  $headers = array(
    t('Name'),
    t('Description'),
    t('Operations')
  );
  $rows = array();

  if (module_exists('drufoo_views')) {
    $webhooks = array();
    $result = db_query("SELECT * FROM {drufoo_webhooks}");
    while ($webhook = db_fetch_array($result)) {
      $webhooks[$webhook['form_hash']] = $webhook;
    }
  }

  foreach ($forms as $form) {
    $links = array(
      'fields' => array(
        'title' => t('Fields'),
        'href' => 'admin/drufoo/forms/'. $form['Hash'] .'/fields'
      ),
      'entries' => array(
        'title' => t('Entries'),
        'href' => 'admin/drufoo/forms/'. $form['Hash'] .'/entries'
      )
    );
    if (isset($webhooks)) {
      if (isset($webhooks[$form['Hash']])) {
        $links['refresh_handshake'] = array(
          'title' => t('Refresh handshake'),
          'href' => 'admin/build/drufoo/webhook/'. $form['Hash'],
          'attributes' => array('title' => t('The handshake is the authentication password used with Wufoo servers to help ensure authenticity.'))
        );
        $links['disintegrate_views'] = array(
          'title' => t('Disable Views integration'),
          'href' => 'admin/build/drufoo/webhook/'. $form['Hash'] .'/delete'
        );
      }
      else {
        $links['integrate_views'] = array(
          'title' => t('Enable Views integration'),
          'href' => 'admin/build/drufoo/webhook/'. $form['Hash']
        );
      }
    }
    $row = array(
      check_plain($form['Name']),
      check_plain($form['Description']),
      theme('links', $links, array())
    );
    $rows[] = $row;
  }

  return theme('table', $headers, $rows);
}

function theme_drufoo_admin_form_fields($fields) {
  $headers = array(
    t('ID'),
    t('Title'),
    t('Type'),
    t('Required')
  );
  $rows = array();

  foreach ($fields as $field) {
    if (isset($field['IsRequired'])) {
      $row = array(
        check_plain($field['ID']),
        check_plain($field['Title']),
        check_plain($field['Type']),
        ($field['IsRequired']) ? t('Yes') : t('No')
      );
      $rows[] = $row;
    }
  }

  return theme('table', $headers, $rows);
}

function theme_drufoo_admin_form_entries($hash, $entries, $fields) {
  // Build $headers array.
  $headers = array();
  foreach ($fields as $key => $field) {
    $headers[$key] = check_plain($field['Title']);
  }

  $rows = array();

  foreach ($entries as $entry) {
    $links = array(
      'view' => array(
        'title' => t('View'),
        'href' => 'admin/drufoo/forms/'. $hash .'/entries/'. $entry['EntryId'] .'/view'
      ),
      // 'edit' => array(
      //   'title' => t('Edit'),
      //   'href' => 'admin/drufoo/forms/'. $hash .'/entries/'. $entry['EntryId'] .'/edit'
      // )
    );

    // Build $row. Loop by $headers to ensure the correct value falls under the
    // correct column.
    $row = array();
    foreach ($headers as $key => $field) {
      if ($key == 'LastUpdated') $key = 'DateUpdated';
      $row[] = check_plain($entry[$key]);
    }

    // Add the operations column to the row.
    $row[] = theme('links', $links);

    $rows[] = $row;
  }

  // Add the operations column here so we don't have to deal with it during the
  // loop above.
  $headers['operations'] = t('Operations');

  return theme('table', array_values($headers), $rows) . theme('pager');
}

function theme_drufoo_admin_entry_view($entry, $fields) {
  $content = array();
  $count = 0;
  foreach ($entry as $key => $field) {
    if ($key == 'DateUpdated') $key = 'LastUpdated';
    $content[$key] = array(
      '#type' => 'item',
      '#title' => check_plain($fields[$key]['Title']),
      '#value' => check_plain($field),
      '#weight' => $count++
    );
  }
  return drupal_render($content);
}

function theme_drufoo_admin_reports_page($reports) {
  $headers = array(
    t('Name'),
    t('Description'),
    t('Operations')
  );
  $rows = array();

  foreach ($reports as $report) {
    $links = array(
      'view' => array(
        'title' => t('View'),
        'href' => 'admin/drufoo/reports/'. $report['Hash'] .'/view'
      )
    );

    $row = array(
      check_plain($report['Name']),
      check_plain($report['Description']),
      theme('links', $links)
    );
    $rows[] = $row;
  }

  return theme('table', $headers, $rows);
}

function theme_drufoo_admin_report_view($report, $snippets) {
  $content = array(
    'details' => array(
      '#type' => 'fieldset',
      '#title' => t('Details'),
      '#collapsible' => TRUE,
      '#collapsed' => (empty($snippets)) ? FALSE : TRUE
    )
  );
  $count = 0;
  foreach (array('Name', 'IsPublic', 'Url', 'Description', 'DateCreated', 'DateUpdated', 'Hash') as $key) {
    $content['details'][$key] = array(
      '#type' => 'item',
      '#title' => check_plain($key),
      '#value' => check_plain($report[$key]),
      '#weight' => $count++
    );
  }
  $output = '';

  foreach ($snippets as $snippet) {
    $output .= $snippet;
  }

  $output .= drupal_render($content);
  return $output;
}
