<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooManagerFields extends DrufooManager {
  public function getFields($hash, $skip_cache = FALSE) {
    $items = $this->getItems();
    if ($skip_cache == FALSE && isset($items[$hash])) {
      return $items[$hash];
    }

    $result = $this->request('forms/' . $hash . '/fields');
    if (isset($result['Fields'])) {
      $fields = array();
      foreach ($result['Fields'] as $field) {
        $fields[$field['ID']] = $field;
      }
      $resource = drufoo_new_object('DrufooResourceFields');
      $resource->setDetails($fields, $hash);
      $this->setItem($hash, $resource);
    }
    $items = $this->getItems();
    return $items[$hash];
  }
}
