<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooManagerForms extends DrufooManager {
  public function getForms($skip_cache = FALSE) {
    if ($skip_cache == FALSE) {
      $items = $this->getItems();
      if ($items !== NULL) {
        return $items;
      }

      // Determine form hashes.
      $hashes = array();
      $result = db_query("SELECT cid FROM {cache_drufoo} WHERE cid LIKE '%s:%%'", 'form');
      while ($data = db_fetch_object($result)) {
        $cid = explode(':', $data->cid);
        if ($data->cid == 'form:'. $cid[1]) {
          $hashes['form:'. $cid[1]] = $cid[1];
        }
      }

      if (!empty($hashes)) {
        // Instead of running any number of $this->getForm() on the hashes
        // acquired above (which would call cache_get each time), we do one
        // database query to load all the forms based on the hashes.
        $result = db_query("SELECT cid, data, created, headers, expire, serialized FROM {cache_drufoo} WHERE cid IN (". db_placeholders(array_keys($hashes), 'varchar') .")", array_keys($hashes));
        while ($cache = db_fetch_object($result)) {
          // If the data is permanent or we're not enforcing a minimum cache lifetime
          // always return the cached data.
          if ($cache->expire == CACHE_PERMANENT || !variable_get('cache_lifetime', 0)) {
            $cache->data = db_decode_blob($cache->data);
            if ($cache->serialized) {
              $cache->data = unserialize($cache->data);
            }
          }
          // If enforcing a minimum cache lifetime, validate that the data is
          // currently valid for this user before we return it by making sure the
          // cache entry was created before the timestamp in the current session's
          // cache timer. The cache variable is loaded into the $user object by
          // sess_read() in session.inc.
          else {
            if (isset($user->cache) && $user->cache > $cache->created) {
              // This cache data is too old and thus not valid for us, ignore it.
              continue;
            }
            else {
              $cache->data = db_decode_blob($cache->data);
              if ($cache->serialized) {
                $cache->data = unserialize($cache->data);
              }
            }
          }

          // Create form resource object.
          $resource = drufoo_new_object('DrufooResourceForm');
          $resource->setDetails($cache->data);
          $this->setItem($hashes[$cache->cid], $resource);
        }

        // Return form items if available. On the occasion there are still no
        // form items available to load a call to Wufoo will be made.
        if ($this->getItems() === NULL) {
          return $this->getItems();
        }
      }
    }

    // Last chance to load all form items. Set $this->items to an empty array
    // to ensure in the case there are no forms at all, we still return an
    // array.
    $this->items = array();

    $result = $this->request('forms');
    if (isset($result['Forms'])) {
      foreach ($result['Forms'] as $form) {
        $resource = drufoo_new_object('DrufooResourceForm');
        $resource->setDetails($form);
        $this->setItem($form['Hash'], $resource);
      }
    }
    return $this->getItems();
  }

  public function getForm($hash, $skip_cache = FALSE) {
    if ($skip_cache == FALSE && $form = $this->getItem($hash)) {
      return $form;
    }

    $resource = drufoo_new_object('DrufooResourceForm', $hash, $skip_cache);

    if ($resource) {
      $this->setItem($hash, $resource);
      return $resource;
    }
  }
}
