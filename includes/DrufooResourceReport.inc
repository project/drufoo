<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooResourceReport extends DrufooResource {
  public $type = 'report';

  protected function uri() {
    return 'reports/'. $this->hash;
  }

  protected function processResponse($response) {
    if (isset($response['Reports']['Report'])) {
      $this->setDetails($response['Reports']['Report']);
      $this->setCache();
      return TRUE;
    }
  }
}
