<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooManagerReports extends DrufooManager {
  public function getReports($skip_cache = FALSE) {
    $items = $this->getItems();

    if ($skip_cache == FALSE) {
      if ($items !== NULL) {
        return $items;
      }
    }

    $this->items = array();

    $result = $this->request('reports');
    if (isset($result['Reports'])) {
      foreach ($result['Reports'] as $report) {
        $resource = drufoo_new_object('DrufooResourceReport');
        $resource->setDetails($report, $report['Hash']);
        $this->setItem($report['Hash'], $resource);
      }
    }

    return $this->getItems();
  }

  public function getReport($hash, $skip_cache = FALSE) {
    $items = $this->getItems();
    if ($skip_cache != FALSE || !isset($items[$hash])) {
      $resource = drufoo_new_object('DrufooResourceReport', $hash, $skip_cache);
      if ($resource) {
        $this->setItem($hash, $resource);
      }
    }
    $items = $this->getItems($hash);
    return $items[$hash];
  }
}
