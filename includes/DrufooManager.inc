<?php

/**
 * @file
 *
 * @author greenSkin
 */

abstract class DrufooManager {
  protected $items;
  protected $request;
  public $pager = NULL;

  public function __construct() {
    $this->init();
  }

  protected function init() {}

  protected function getItems() {
    return $this->items;
  }

  protected function getItem($hash) {
    if (!isset($this->items[$hash])) {
      return FALSE;
    }
    return $this->items[$hash];
  }

  protected function setItem($hash, $data) {
    $this->items[$hash] = $data;
  }

  protected function request($call, $query = array(), $data = NULL, $ext = 'xml') {
    $requester = drufoo_get_requester();
    return $requester->request($call, $query, $data, $ext);
  }

  public function attachPager() {
    if ($this->pager == FALSE) {
      $this->pager = drufoo_new_object('DrufooPager');
    }
  }
}
