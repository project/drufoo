<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooPager {
  protected $element = 0;
  protected $limit = 25;

  public function setLimit($limit) {
    $this->limit = ($limit <= 100) ? $limit : 100;
  }

  public function setElement($element) {
    $this->element = $element;
  }

  public function query($count) {
    global $pager_page_array, $pager_total, $pager_total_items;

    $page = isset($_GET['page']) ? $_GET['page'] : '';
    $pager_page_array = explode(',', $page);
    $pager_total_items[$this->element] = $count;
    $pager_total[$this->element] = ceil($pager_total_items[$this->element] / $this->limit);
    $pager_page_array[$this->element] = max(0, min((int)$pager_page_array[$this->element], ((int)$pager_total[$this->element]) - 1));

    return array(
      'pageStart' => rawurlencode($pager_page_array[$this->element] * $this->limit),
      'pageSize' => rawurlencode($this->limit)
    );
  }
}
