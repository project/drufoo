<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooRequestPut extends DrufooRequest {
  protected function setCurlOptions($handle, $data = NULL) {
    parent::setCurlOptions($handle, $data);

    $fields = ($data) ? drupal_query_string_encode($data) : '';

    curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
    // curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Length: ' . strlen($fields)));
    curl_setopt($handle, CURLOPT_POSTFIELDS, $fields);
  }
}
