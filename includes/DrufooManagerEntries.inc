<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooManagerEntries extends DrufooManager {
  public $filter = NULL;

  protected function getItems($hash) {
    $items = parent::getItems();
    return (isset($items[$hash])) ? $items[$hash] : NULL;
  }

  /**
   * Get a form's entries from static cache and/or from Wufoo's servers.
   *
   * @param $hash
   *   A form hash.
   * @param $skip_cache
   *   Whether to skip cache and load directly from Wufoo servers.
   *
   * @return
   *   Array of DrufooResourceEntry objects.
   */
  public function getEntries($hash, $skip_cache = FALSE) {
    $items = $this->getItems($hash);

    if ($skip_cache == FALSE) {
      if ($items !== NULL) {
        return $items;
      }

      // Try to load cached entries for the form.
      $result = db_query("SELECT cid FROM {cache_drufoo} WHERE cid LIKE '%s%%'", 'entry:'. $hash .':');
      while ($data = db_fetch_object($result)) {
        if ($cache = cache_get($data->cid, 'cache_drufoo')) {
          $cid_array = preg_split('/:/', $data->cid);
          $entry_id = $cid_array[2];
          $resource = drufoo_new_object('DrufooResourceEntry');
          $resource->setDetails($cache->data, $hash, $entry_id);
          $this->setItem($hash, $entry_id, $resource);
        }
      }

      $items = $this->getItems($hash);
      if ($items !== NULL) {
        return $items;
      }
    }

    $result = $this->request('forms/' . $hash . '/entries', $this->buildQuery($hash));
    if (isset($result['Entries'])) {
      foreach ($result['Entries'] as $entry) {
        $resource = drufoo_new_object('DrufooResourceEntry');
        $resource->setDetails($entry, $hash, $entry['EntryId']);
        $this->setItem($hash, $entry['EntryId'], $resource);
      }
    }

    return $this->getItems($hash);
  }

  public function getEntry($hash, $entry_id, $skip_cache = FALSE) {
    $items = $this->getItems($hash);
    if ($skip_cache != FALSE || !isset($items[$entry_id])) {
      $resource = drufoo_new_object('DrufooResourceEntry', $hash, $entry_id, $skip_cache);
      if ($resource) {
        $this->setItem($hash, $entry_id, $resource);
      }
    }
    $items = $this->getItems($hash);
    return $items[$entry_id];
  }

  protected function setItem($hash, $entry_id, $data) {
    $items = $this->getItems($hash);
    $items[$entry_id] = $data;
    parent::setItem($hash, $items);
  }

  public function countEntries($hash, $reset = FALSE) {
    static $counts = array();

    if (empty($counts) && $reset === FALSE) {
      $counts = variable_get('drufoo_entry_counts', array());
    }

    if ($reset !== FALSE || !isset($counts[$hash])) {
      $result = $this->request('forms/' . $hash . '/entries/count');
      $counts[$hash] = ($result['EntryCount']) ? $result['EntryCount'] : FALSE;
      variable_set('drufoo_entry_counts', $counts);
    }

    return $counts[$hash];
  }

  public function buildQuery($hash) {
    $query = array();
    if ($this->pager != NULL) {
      $query += $this->pager->query($this->countEntries($hash));
    }
    if ($this->filter != NULL) {
      $query += $this->filter->query();
    }
    return $query;
  }

  public function attachFilter() {
    if ($this->filter == NULL) {
      $this->filter = drufoo_new_object('DrufooFilter');
    }
  }
}
