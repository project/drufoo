<?php

/**
 * @file
 *
 * @author greenSkin
 */

abstract class DrufooResource {
  public $type;
  public $hash;
  protected $details = FALSE;

  public function __construct($hash = NULL, $skip_cache = FALSE) {
    if ($hash !== NULL) {
      $this->hash = $hash;
      $this->fetchDetails($skip_cache);
    }
  }

  abstract protected function uri();

  public function fetchDetails($skip_cache = FALSE) {
    if (!$this->hash) {
      return FALSE;
    }

    if ($skip_cache == FALSE && $cache = $this->getCache()) {
      $this->setDetails($cache->data);
    }
    else {
      $response = $this->request($this->uri());
      $this->processResponse($response);
    }
  }

  public function setDetails($details) {
    $this->details = $details;
    if (isset($details['Hash'])) {
      $this->hash = $details['Hash'];
    }
    $this->setCache();
  }

  public function getDetails() {
    if (!isset($this->details)) {
      $this->fetchDetails();
    }
    return $this->details;
  }

  public function getCache() {
    if ($cache = cache_get($this->type . ':' . $this->hash, 'cache_drufoo')) {
      $this->details = $cache->data;
      return $cache;
    }
  }

  public function setCache() {
    if ($this->details && $this->hash) {
      cache_set($this->type . ':' . $this->hash, $this->getDetails(), 'cache_drufoo');
      return TRUE;
    }
  }

  protected function request($call, $query = array(), $data = NULL, $ext = 'xml') {
    $requester = drufoo_get_requester();
    return $requester->request($call, $query, $data, $ext);
  }

  abstract protected function processResponse($response);
}
