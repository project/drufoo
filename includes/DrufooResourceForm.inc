<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooResourceForm extends DrufooResource {
  public $type = 'form';

  protected function uri() {
    return $this->type . 's/' . $this->hash;
  }

  protected function processResponse($response) {
    if (isset($response['Forms']['Form'])) {
      $this->setDetails($response['Forms']['Form']);
      $this->setCache();
      return TRUE;
    }
  }
}
