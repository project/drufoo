<?php

/**
 * @file
 *
 * @author greenSkin
 */

abstract class DrufooRequest {
  protected $url;
  protected $username;
  protected $key;
  protected $handle;

  public function __construct() {
    $this->username = variable_get('drufoo_username', '');
    $this->key = variable_get('drufoo_key', '');
    $this->setURL('https://'. $this->username .'.wufoo.com/api/v3');
  }

  protected function setURL($url) {
    $this->url = $url;
  }

  protected function getHandle() {
    return $this->handle;
  }

  protected function setHandle($url) {
    $this->handle = curl_init($url);
    return $this->handle;
  }

  protected function setCurlOptions($handle) {
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($handle, CURLOPT_USERPWD, $this->key .':footastic');
    curl_setopt($handle, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($handle, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($handle, CURLOPT_USERAGENT, 'Drufoo');
  }

  protected function closeCurl($handle) {
    curl_close($handle);
  }

  public function request($call, $query = array(), $data = NULL, $ext = 'xml') {
    if ($this->username && $this->key) {
      $url = $this->url . "/$call.$ext";
      if (is_array($query) && !empty($query)) {
        $params = array();
        foreach ($query as $key => $value) {
          $params[] = $key . '=' . $value;
        }
        $url .= '?' . implode('&', $params);
      }

      $this->setHandle($url);

      $this->setCurlOptions($this->getHandle(), $data);

      $response = curl_exec($this->getHandle());
      $resultStatus = curl_getinfo($this->getHandle());

      if ($resultStatus['http_code'] != 200) {
        if ($resultStatus['http_code'] == 404) {
          // Page not found.
          watchdog('drufoo', 'Status: @status', array('@status' => var_export($resultStatus, TRUE)), WATCHDOG_NOTICE);
          return FALSE;
        }
        watchdog('drufoo', 'Call failed: @response<br />Status: @status', array('@response' => $response, '@status' => var_export($resultStatus, TRUE)), WATCHDOG_NOTICE);
        return $response;
      }

      $this->closeCurl($this->getHandle());
      return $this->transposeData($response, $ext);
    }
  }

  protected function transposeData($data, $ext = 'xml') {
    switch ($ext) {
      case 'xml':
        $xml = simplexml_load_string($data);
        return $this->simplexml2array($xml);
      case 'json':
        return json_decode($data, TRUE);
    }
  }

  protected function simplexml2array($xml, $root = TRUE) {
    if (!$xml->children()) {
      return (string)$xml;
    }

    $array = array();
    foreach ($xml->children() as $element => $node) {
      $totalElement = count($xml->{$element});

      // Has attributes.
      if ($attributes = $node->attributes()) {
        $data = array(
          'attributes' => array(),
          'value' => (count($node) > 0) ? $this->simplexml2array($node, FALSE) : (string)$node
        );

        foreach ($attributes as $attr => $value) {
          $data['attributes'][$attr] = (string)$value;
        }

        if ($totalElement > 1) {
          $array[] = $data;
        }
        else {
          $array[$element] = $data;
        }
      }
      // Just a value.
      else {
        if ($totalElement > 1) {
          $array[] = $this->simplexml2array($node, FALSE);
        }
        else {
          $array[$element] = $this->simplexml2array($node, FALSE);
        }
      }
    }

    if ($root) {
      return array($xml->getName() => $array);
    }
    return $array;
  }
}
