<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooResourceFields extends DrufooResource {
  public $type = 'fields';

  protected function uri() {
    return 'forms/' . $this->hash . '/fields';
  }

  protected function processResponse($response) {
    if (isset($response['Fields'])) {
      $fields = array();
      foreach ($response['Fields'] as $field) {
        $fields[$field['ID']] = $field;
      }
      $this->setDetails($fields);
      $this->setCache();
      return TRUE;
    }
  }

  public function setDetails($details, $hash = NULL) {
    $this->details = $details;
    if ($hash !== NULL) {
      $this->hash = $hash;
    }
    $this->setCache();
  }
}
