<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooManagerWidgets extends DrufooManager {
  public $filter = NULL;

  protected function getItems($hash) {
    $items = parent::getItems();
    return (isset($items[$hash])) ? $items[$hash] : NULL;
  }

  /**
   * Get a report's widgets from static cache and/or from Wufoo's servers.
   *
   * @param $hash
   *   A form hash.
   * @param $skip_cache
   *   Whether to skip cache and load directly from Wufoo servers.
   *
   * @return
   *   Array of DrufooResourceWidget objects.
   */
  public function getWidgets($hash, $skip_cache = FALSE) {
    if ($skip_cache == FALSE) {
      $items = $this->getItems($hash);
      if ($items !== NULL) {
        return $items;
      }

      // Try to load cached entries for the form.
      $result = db_query("SELECT cid FROM {cache_drufoo} WHERE cid LIKE '%s%%'", 'widget:'. $hash .':');
      while ($data = db_fetch_object($result)) {
        if ($cache = cache_get($data->cid, 'cache_drufoo')) {
          $cid_array = preg_split('/:/', $data->cid);
          $entry_id = $cid_array[2];
          $resource = drufoo_new_object('DrufooResourceWidget');
          $resource->setDetails($cache->data, $hash, $entry_id);
          $this->setItem($hash, $entry_id, $resource);
        }
      }

      $items = $this->getItems($hash);
      if ($items !== NULL) {
        return $items;
      }
    }

    $result = $this->request('reports/' . $hash . '/widgets');
    if (isset($result['Widgets'])) {
      foreach ($result['Widgets'] as $widget) {
        $resource = drufoo_new_object('DrufooResourceWidget');
        $resource->setDetails($widget, $hash, $widget['Hash']);
        $this->setItem($hash, $widget['Hash'], $resource);
      }
    }

    return $this->getItems($hash);
  }

  protected function setItem($hash, $widget_hash, $data) {
    $items = $this->getItems($hash);
    $items[$widget_hash] = $data;
    parent::setItem($hash, $items);
  }
}
