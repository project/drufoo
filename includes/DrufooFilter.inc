<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooFilter {
  protected $match;
  protected $filters = array();

  public function __construct() {
    // Apply any filter settings set in session as defaults.
    if (isset($_SESSION['drufoo']['filter']) && !empty($_SESSION['drufoo']['filter'])) {
      if (isset($_SESSION['drufoo']['filter']['match']) && $_SESSION['drufoo']['filter']['match'] == 'OR') {
        $this->useMatchOr();
      }
      foreach ($_SESSION['drufoo']['filter']['filters'] as $filter) {
        list($id, $operator, $value) = $filter;
        $this->addFilter($id, $operator, $value);
      }
    }
    else {
      $this->useMatchAnd();
    }
  }

  public function useMatchAnd() {
    $this->match = 'AND';
  }

  public function useMatchOr() {
    $this->match = 'OR';
  }

  public function addFilter($id, $operator, $value) {
    $operators = drufoo_filter_operators();
    if (isset($operators[$operator])) {
      $number = count($this->filters) + 1;
      $this->filters['Filter' . $number] = rawurlencode($id) . '+' . $operator . '+' . rawurlencode($value);
    }
  }

  public function clearFilters() {
    $this->filters = array();
  }

  public function getFilters() {
    return $this->filters;
  }

  public function query() {
    return $this->getFilters();
  }
}
