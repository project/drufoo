<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooResourceWidget extends DrufooResource {
  public $type = 'widget';
  public $widgetHash;

  protected function uri() {}

  protected function request() {}

  protected function processResponse($response) {}

  public function setDetails($details, $hash, $widget_hash) {
    $this->details = $details;
    $this->hash = $hash;
    $this->widgetHash = $widget_hash;
    $this->setCache();
  }

  public function getSnippet() {
    return '<script type="text/javascript">var host = (("https:" == document.location.protocol) ? "https://" : "http://");document.write(unescape("%3Cscript src=\'" + host + "'. variable_get('drufoo_username', '') .'.wufoo.com/scripts/widget/embed.js?w='. $this->widgetHash .'\' type=\'text/javascript\'%3E%3C/script%3E"));</script>';
  }
}
