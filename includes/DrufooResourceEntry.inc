<?php

/**
 * @file
 *
 * @author greenSkin
 */

class DrufooResourceEntry extends DrufooResource {
  public $type = 'entry';
  public $entryId;

  public function __construct($hash = NULL, $entry_id = NULL, $skip_cache = FALSE) {
    if ($hash !== NULL) {
      $this->hash = $hash;
    }
    if ($hash !== NULL && $entry_id !== NULL) {
      $this->entryId = $entry_id;
      $this->fetchDetails($skip_cache);
    }
  }

  protected function uri() {
    return 'forms/' . $this->hash . '/entries';
  }

  public function fetchDetails($skip_cache = FALSE) {
    if (!$this->entryId) {
      return FALSE;
    }
    parent::fetchDetails($skip_cache);
  }

  public function setDetails($details, $hash = NULL, $entry_id = NULL) {
    $this->details = $details;
    if ($hash !== NULL) {
      $this->hash = $hash;
    }
    if ($entry_id !== NULL) {
      $this->entryId = $entry_id;
    }
    $this->setCache();
  }

  public function getCache() {
    if ($cache = cache_get($this->type . ':' . $this->hash . ':' . $this->entryId, 'cache_drufoo')) {
      $this->details = $cache->data;
      return $cache;
    }
  }

  public function setCache() {
    if ($this->details && $this->hash) {
      cache_set($this->type . ':' . $this->hash . ':' . $this->entryId, $this->getDetails(), 'cache_drufoo');
      return TRUE;
    }
  }

  protected function processResponse($response) {
    if ($this->entryId && isset($response['Entries'])) {
      foreach ($response['Entries'] as $entry) {
        if ($entry['EntryId'] == $this->entryId) {
          $this->setDetails($entry);
          $this->setCache();
          return TRUE;
        }
      }
    }
  }
}
